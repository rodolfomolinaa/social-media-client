import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';
// import store from '../store';

Vue.use(Router);

// export default new Router({
//   // mode: 'hash', // https://router.vuejs.org/api/#mode,
//   mode: 'history', // https://router.vuejs.org/api/#mode
//   linkActiveClass: 'open active',
//   scrollBehavior: () => ({ y: 0 }),
//   routes,
// })

const router = new Router({
  // mode: 'hash', // https://router.vuejs.org/api/#mode,
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes,
});

export default router;
