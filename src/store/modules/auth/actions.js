import router from '../../../router';
import store from '../..';

export default {
  authenticate({ commit }, payload) {
    commit('authenticate', {
      access_token: payload.access_token,
      user: payload.user,
    });

    if (store.state.auth.user.change_password) {
      router.push({ name: 'CustomPassword' });
    } else {
      router.push({ name: 'Dashboard' });
    }
  },
  logout({ commit }) {
    commit('logout');
    router.push({ name: 'Login' });
  },
};
