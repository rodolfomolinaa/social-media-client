import axios from 'axios';

export const index = async (pagination) => {
  const { data } = await axios.get('/clients', {
    params: {
      per_page: pagination.per_page,
      page: pagination.page,
    },
  });
  return data;
};


export const store = async (form) => {
  const { data } = await axios.post('/clients', form);
  return data;
};

export const show = async (id) => {
  const { data } = await axios.get(`/clients/${id}`);
  return data;
};

export const update = async (params) => {
  const { data } = await axios.put(`/clients/${params.id}`, params);
  return data;
};

export const destroy = async (id) => {
  const { data } = await axios.delete(`/clients/${id}`);
  return data;
};
