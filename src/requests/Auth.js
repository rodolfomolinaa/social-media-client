import axios from 'axios';

export const login = async ({ email, password }) => {
  const { data } = await axios
    .post('/login', {
      email: email.trim(),
      password,
    });
  return data;
};

export const logout = async () => {
  const { data } = await axios
    .post('/logout');
  return data;
};
