import axios from 'axios';

export const index = async (pagination) => {
  const { data } = await axios.get('/users', {
    params: {
      per_page: pagination.per_page,
      page: pagination.page,
    },
  });
  return data;
};


export const store = async (form) => {
  const { data } = await axios.post('/users', form);
  return data;
};

export const show = async (id) => {
  const { data } = await axios.get(`/users/${id}`);
  return data;
};

export const update = async (params) => {
  const { data } = await axios.put(`/users/${params.id}`, params);
  return data;
};

export const destroy = async (id) => {
  const { data } = await axios.delete(`/users/${id}`);
  return data;
};

export const customPassword = async (form) => {
  const { data } = await axios.post('/users/custom-password', form);
  return data;
};
