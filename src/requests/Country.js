import axios from 'axios';

export const index = async (pagination) => {
  const { data } = await axios.get('/countries', {
    params: {
      per_page: pagination.per_page,
      page: pagination.page,
    },
  });
  return data;
};


export const store = async (form) => {
  const { data } = await axios.post('/countries', form);
  return data;
};

export const show = async (id) => {
  const { data } = await axios.get(`/countries/${id}`);
  return data;
};

export const update = async (params) => {
  const { data } = await axios.put(`/countries/${params.id}`, params);
  return data;
};

export const destroy = async (id) => {
  const { data } = await axios.delete(`/countries/${id}`);
  return data;
};

export const select = async () => {
  const { data } = await axios.get('/countries/select');
  return data;
};
