import axios from 'axios';

export const index = async () => {
  const { data } = await axios.get('/projects/list');
  return data;
};


export const select = async () => {
  const { data } = await axios.get('/roles/select');
  return data;
};
