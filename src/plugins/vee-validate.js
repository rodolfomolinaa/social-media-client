// import Vue from 'vue';
// // import * as VeeValidate from 'vee-validate';
// import { ValidationProvider, extend } from 'vee-validate';

// extend('secret', {
//   validate: (value) => value === 'example',
//   message: 'This is not the magic word',
// });

// Vue.component('ValidationProvider', ValidationProvider);

// const config = {
//   locale: 'es',
// };
// Vue.use(VeeValidate, config);
// import VeeValidate, { ValidationProvider } from 'vee-validate';
// import es from 'vee-validate/dist/locale/es';

// Vue.use(VeeValidate, {
//   locale: 'es',
// });
// ValidationProvider.localize('es', es);

import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';
import * as es from 'vee-validate/dist/locale/es';

Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'vvFields',

  locale: 'es',
  events: 'change|blur|keyup',
  classes: true,
  classNames: {
    valid: 'is-valid',
    invalid: 'is-invalid',
  },
});

Validator.localize('es', es);
